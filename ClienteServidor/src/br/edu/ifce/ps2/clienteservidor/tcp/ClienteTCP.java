package br.edu.ifce.ps2.clienteservidor.tcp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

public class ClienteTCP {
	public static void main(String[] args) {
		try {
			Socket cliente;
			InetAddress serverIP = 
				InetAddress.getByAddress(new byte[] {127,0,0,1});
			cliente = new Socket(serverIP, 12345);
			System.out.println("Conexão estabelecida");
			ObjectOutputStream saida = 
				new ObjectOutputStream(cliente.getOutputStream());
			ObjectInputStream entrada =
				new ObjectInputStream(cliente.getInputStream());
			while(true) {
				String msg = 
						JOptionPane.showInputDialog("Mensagem:");
				saida.writeUTF(msg);
				
				String resposta = entrada.readUTF();
				JOptionPane.showMessageDialog(null, resposta);
			}
					
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
