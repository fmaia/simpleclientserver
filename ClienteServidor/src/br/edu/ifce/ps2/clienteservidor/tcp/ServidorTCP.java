package br.edu.ifce.ps2.clienteservidor.tcp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class ServidorTCP {

	public static void main(String[] args) {
		try {
			ServerSocket servidor = new ServerSocket(12345);
			System.out.println("Servidor ouvindo em 12345.");
			
			while(true) {
				Socket cliente = servidor.accept();
				System.out.println("Cliente conectado.");
				ObjectInputStream entrada = 
					new ObjectInputStream(cliente.getInputStream());
				ObjectOutputStream saida = 
					new ObjectOutputStream(cliente.getOutputStream());
				while(true) {
					String msg = entrada.readUTF();
					JOptionPane.showMessageDialog(null, msg);
					String resposta = 
						JOptionPane.showInputDialog("Resposta:");
					saida.writeUTF(resposta);
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
