package br.edu.ifce.ps2.clienteservidor.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.swing.JOptionPane;

public class ClienteUDP {
	public static void main(String[] args) {
		try {
			DatagramSocket cliente = new DatagramSocket();
			InetAddress serverIP 
				= InetAddress.getByAddress(new byte[]{127,0,0,1});
			byte[] sendBuffer;
			byte[] receiveBuffer = new byte[1024];
			String msg = JOptionPane.showInputDialog("Mensagem:");
			sendBuffer = msg.getBytes();
			DatagramPacket sendPacket 
				= new DatagramPacket(sendBuffer,
						sendBuffer.length, serverIP, 12346);
			System.out.println("Enviando...");
			cliente.send(sendPacket);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
