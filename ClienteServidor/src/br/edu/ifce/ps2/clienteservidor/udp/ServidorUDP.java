package br.edu.ifce.ps2.clienteservidor.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;

public class ServidorUDP {
	public static void main(String[] args) {
		try {
			DatagramSocket servidor = new DatagramSocket(12346);
			System.out.println("Servidor ouvindo porta 12346");
			byte[] receiveBuffer = new byte[1024];
			byte[] sendBuffer;
			while(true) {
				DatagramPacket receivePacket = 
						new DatagramPacket(receiveBuffer, 
								receiveBuffer.length); 
				System.out.println("Esperando por datagramas...");
				servidor.receive(receivePacket);
				System.out.println("Datagrama recebido.");
				String msg = new String(receivePacket.getData());
				System.out.println("Mensagem: " + msg);
				Arrays.fill(receiveBuffer, (byte)0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
